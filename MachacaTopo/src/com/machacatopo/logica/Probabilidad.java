package com.machacatopo.logica;
/**
 * 
 * @author Leonardo Ortega Hernandez - leon9894@gmail.com
 *
 */
public class Probabilidad {
	// orden de valores segun tipo de topo, {BASICO, AGIL, RESISTENTE, NINJA,
	// BONUS}
	public static float[][] tipoTopo = { { Topo.AGIL, 0.2f },
			{ Topo.BASICO, 0.2f }, { Topo.NINJA, 0.2f },
			{ Topo.RESISTENTE, 0.2f }, { Topo.BONUS, 0.2f } };

	public static float[][] getVariableAleatorea(float[][] variable) {
        for (int i = 0; i < variable.length; i++) {
            for (int j = i + 1; j < variable.length; j++) {
                if (variable[i][1] > variable[j][1]) {
                	float[] aux = variable[i];
                    variable[i] = variable[j];
                    variable[j] = aux;
                }
            }
        }
        return variable;
    }

    public static int simular(float[][] variableAleatoria, float valor) {
    	float intervaloInferior = 0;
    	float intervaloSuperior = variableAleatoria[0][1];;
        for (int i = 0; i < variableAleatoria.length; i++) {
            if (valor >= intervaloInferior && valor < intervaloSuperior) {
                return (int) variableAleatoria[i][0];
            }
            intervaloInferior = intervaloSuperior;
            intervaloSuperior += variableAleatoria[i + 1][1];
        }
        return -1;
    }

    public static float sumaProbabilidad(float[][] variableAleatoria) {
    	float suma = 0;
        for (int i = 0; i < variableAleatoria.length; i++) {
            suma += variableAleatoria[i][1];
        }
        return suma;
    }

    public static int getIndex(float[][] variableAleatoria, float key) {
        for (int i = 0; i < variableAleatoria.length; i++) {
            if (key == variableAleatoria[i][0]) {
                return i;
            }
        }
        return -1;
    }

    public static void ajustarProbablidad(float[][] variableAleatoria) {
    	float error = 1 - sumaProbabilidad(variableAleatoria);
    	float errorDistribuido = error / variableAleatoria.length;
        for (int i = 0; i < variableAleatoria.length; i++) {
            variableAleatoria[i][1] += errorDistribuido;
        }
    }

    public static void ajustarProbablidad(float[][] variableAleatoria, int index, float suma) {
    	float error = 1 - suma;
        for (int i = 0; i < variableAleatoria.length; i++) {
            if (index != i) {
                variableAleatoria[i][1] += variableAleatoria[i][1] * error;
            }
        }
        suma = sumaProbabilidad(variableAleatoria);
        if (!(suma >= 0.99999993 && suma <= 1.000001)) {
            ajustarProbablidad(variableAleatoria, index, suma);
        }
    }
    //==========================================================================
    //Objectos
    //====================================================================
    public static Object[][] getVariableAleatorea(Object[][] variable) {
        for (int i = 0; i < variable.length; i++) {
            for (int j = i + 1; j < variable.length; j++) {
                if ((Float) (variable[i][1]) > (Float) (variable[j][1])) {
                    Object[] aux = variable[i];
                    variable[i] = variable[j];
                    variable[j] = aux;
                }
            }
        }
        return variable;
    }

    public static Object simular(Object[][] variableAleatoria, float valor) {
    	float intervaloInferior = 0;
    	float intervaloSuperior = (Float) (variableAleatoria[0][1]);
        for (int i = 0; i < variableAleatoria.length; i++) {
            if (valor >= intervaloInferior && valor < intervaloSuperior) {
                return variableAleatoria[i][0];
            }
            intervaloInferior = intervaloSuperior;
            intervaloSuperior += (Float) (variableAleatoria[i + 1][1]);
        }
        return -1;
    }

    public static float sumaProbabilidad(Object[][] variableAleatoria) {
        float suma = 0;
        for (int i = 0; i < variableAleatoria.length; i++) {
            suma += (Float) (variableAleatoria[i][1]);
        }
        return suma;
    }



    public static void ajustarProbablidad(Object[][] variableAleatoria) {
        float error = 1 - sumaProbabilidad(variableAleatoria);
        float errorDistribuido = error / variableAleatoria.length;
        for (int i = 0; i < variableAleatoria.length; i++) {
            variableAleatoria[i][1] = (Float) (variableAleatoria[i][1]) + errorDistribuido;
        }
    }

    public static void ajustarProbablidad(Object[][] variableAleatoria, int index, float suma) {
        float error = 1 - suma;
        for (int i = 0; i < variableAleatoria.length; i++) {
            if (index != i) {
                variableAleatoria[i][1] = (Float) (variableAleatoria[i][1]) + (Float) (variableAleatoria[i][1]) * error;
            }
        }
        suma = sumaProbabilidad(variableAleatoria);
        if (!(suma >= 0.99999993 && suma <= 1.000001)) {
            ajustarProbablidad(variableAleatoria, index, suma);
        }
    }

}
