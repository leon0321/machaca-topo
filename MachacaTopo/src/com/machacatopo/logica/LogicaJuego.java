package com.machacatopo.logica;

import java.util.Date;

import com.machacatopo.logica.tools.Time;
import com.machacatopo.vista.FactoryTopoSprite;

import android.util.Log;

/**
 * 
 * @author Leonardo Ortega Hernandez - leon9894@gmail.com
 *
 */
public class LogicaJuego {
	public static boolean FIN_PARTIDA = false;
	private Tablero tablero = Tablero.get();
	private static LogicaJuego logicaJuego;

	private LogicaJuego() {

	}

	public static LogicaJuego get() {
		if (logicaJuego == null) {
			logicaJuego = new LogicaJuego();
		}
		return logicaJuego;
	}

	public void aplicarRegla(Hecho hecho) {
		Herramienta h = hecho.getHerramienta();
		Topo topo = hecho.getTopo();
		if (hecho.getTipo() == Hecho.ESCAPO_TOPO) {
			if (topo.getTipo() != Topo.BONUS && topo.getTipo() != Topo.NINJA) {
				tablero.restarIntento();
			}
		} else if (hecho.getTipo() == Hecho.GOLPE_TOPO) {

			if (topo.getTipo() == Topo.NINJA) {
				tablero.restarIntento();
				topo.setResistencia(0);
			} else if (topo.getTipo() == Topo.BONUS) {
				topo.setResistencia(0);
			} else {
				topo.golpear(h.getPuntosNegativos());
				Log.i("Resistencia:", "" + topo.getResistencia());
				if (!topo.isVivo()) {
					tablero.sumarPuntos(topo.getPuntaje() * h.getFactorIncremento());
				}
			}
		}
		if (tablero.getIntentosRestantes() < 0) {
			FIN_PARTIDA = true;
			FactoryTopoSprite.getInstance().pause();
		}
	}

	public void newGame(){
		FIN_PARTIDA = false;
		tablero.setIntentosRestantes(10);
		Time.timeGame = new Date().getTime();
	}
	
	public void registrarHecho(Hecho hecho) {

	}


}
