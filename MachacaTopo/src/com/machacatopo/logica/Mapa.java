package com.machacatopo.logica;

import java.util.ArrayList;
import java.util.Random;

import com.machacatopo.logica.tools.C;

import android.util.Log;
/**
 * 
 * @author Leonardo Ortega Hernandez - leon9894@gmail.com
 *
 */
public class Mapa {

	// ===========================================================
	// Constants
	// ===========================================================
	private static Mapa instance = null;
	public static final int VACIO = C.value();
	public static final int FILL = C.value();
	public static final int WIDTH_FRAME = 73, HEIGHT_FRAME = 47;
	public static final int ROW = 9, COLUMN = 8;
	private static double []factoresX;
	private static final int X = 1;
	private static final int Y = 0;
	private static final int PADDING_LEFT = 110;
	private static final int PADDING_TOP = -10;


	// ===========================================================
	// Fields
	// ===========================================================
	private ArrayList<int[]> disponibles = new ArrayList<int[]>();
	private boolean full = false;
	private int planoCartesiano[][];
	private int BorderLeft = 1;
	private int BorderRight = 30;
	// ===========================================================
	// Constructors
	// ===========================================================

	private Mapa() {
		
		factoresX = new double[]{-14,-10,-5,-2,2,5,10,14};
		planoCartesiano = new int[ROW][COLUMN];
		for (int i = 0; i < ROW; i++) {
			for (int j = 0; j < COLUMN; j++) {
				disponibles.add(new int[] { i, j });
				planoCartesiano[i][j] = VACIO;
			}
		}


//		int j = 0;
//		int i = 0;
//		planoCartesiano[i][j++] = FILL;
//		planoCartesiano[i][j++] = FILL;
//		planoCartesiano[i][j++] = FILL;
//		planoCartesiano[i][j++] = FILL;
//		planoCartesiano[i][j++] = FILL;
//		j = 0;
//		planoCartesiano[i++][COLUMN - 1] = FILL;
//		planoCartesiano[i++][COLUMN - 1] = FILL;
//		planoCartesiano[i++][COLUMN - 1] = FILL;
//		planoCartesiano[i++][COLUMN - 1] = FILL;
//		planoCartesiano[i++][COLUMN - 1] = FILL;
//		planoCartesiano[i++][COLUMN - 1] = FILL;
//		planoCartesiano[i++][COLUMN - 1] = FILL;
//		planoCartesiano[i++][COLUMN - 1] = FILL;
//		planoCartesiano[i++][COLUMN - 1] = FILL;
//		planoCartesiano[i++][COLUMN - 1] = FILL;
//		planoCartesiano[i++][COLUMN - 1] = FILL;
//		planoCartesiano[i++][COLUMN - 1] = FILL;
//		planoCartesiano[i++][COLUMN - 1] = FILL;
//		planoCartesiano[i++][COLUMN - 1] = FILL;
//		
//		planoCartesiano[12][0] = FILL;
//		planoCartesiano[13][0] = FILL;
//		planoCartesiano[13][1] = FILL;
//		planoCartesiano[13][2] = FILL;


	}

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	public static Mapa getInstance() {
		if (instance == null) {
			instance = new Mapa();
		}
		return instance;
	}

	public int[][] getPlanoCartesiano() {
		return planoCartesiano;
	}

	public void setPlanoCartesiano(int planoCartesiano[][]) {
		this.planoCartesiano = planoCartesiano;
	}

	public int getElemento(int[] posicion) {
		return planoCartesiano[posicion[0]][posicion[1]];
	}

	/**
	 * asigna un numero, para identificar que elemento esta en esa posicion
	 * 
	 * @param coord
	 * @param tipoElemento
	 */
	public void setElemento(int[] coord, int tipoElemento) {
		planoCartesiano[coord[0]][coord[1]] = tipoElemento;
	}

	public ArrayList<int[]> getDisponibles() {
		disponibles.clear();
		for (int i = 0; i < ROW; i++) {
			for (int j = 0; j < COLUMN; j++) {
				if (planoCartesiano[i][j] == VACIO) {
					disponibles.add(new int[] { i, j });
				}
			}
		}
		return disponibles;
	}

	public boolean isFull() {
		full = getDisponibles().isEmpty();
		return full;
	}

	// ===========================================================
	// Methods
	// ===========================================================

	/**
	 * Metodo que convierte un par de coordenadas en pixeles a coordenadas
	 * cartesianas
	 * 
	 * @param coorPixel
	 * @return
	 */
	public static int[] convertirPxToCoor(int coorPixel[]) {
		int coord[] = new int[2];

		coord[0] = (coorPixel[0] / WIDTH_FRAME) - 1;
		coord[1] = (coorPixel[1] / HEIGHT_FRAME) - 1;
		return coord;
	}

	/**
	 * Metodo que convierte de cordenadas cartesianas a pixeles.
	 * 
	 * @param coordenadas
	 *            del plano cartesiano.
	 * @return coordenadas en pixeles.
	 */
	public static int[] convertirCoorToPx(int[] coor) {
		double g = factoresX[coor[X]];
		double x = (int)((double)(coor[X]*WIDTH_FRAME) + (double)coor[Y]*g/**(Math.pow(1 + 0.005, coor[0])*/ );
		double y = (int)((coor[Y] * HEIGHT_FRAME)*(Math.pow(1 + 0.005, coor[Y]/*+ (double)coor[0]**/)));
		///Log.i("coordenada",coor[0]*WIDTH_FRAME + " ------> " + x);
		return new int[] {PADDING_LEFT +  (int)x, PADDING_TOP +  (int)y };
	}

	public boolean posicionIsEmpty(int coord[]) {
		if (planoCartesiano[coord[0]][coord[1]] > VACIO) {
			return false;
		}
		return true;
	}

	public int[] generarPosicion() {
		getDisponibles();
		int posicionRandom = 0;
		if (!disponibles.isEmpty()) {
			if (disponibles.size() >= 1) {
				posicionRandom = new Random().nextInt(disponibles.size());
				return disponibles.get(posicionRandom);
			}
			return disponibles.get(0);
		}
		return null;
	}
	
	public static int nose(int y, int pixX,float r){
		pixX = (int)Math.pow((1 + r), y);
		return pixX;
	}
}
