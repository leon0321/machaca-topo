package com.machacatopo.logica.tools;

public class Time {
	public static long pause = 0;
	public static long resume = 0;
	public static long timePaused = 0;
	public static long timeGame = 0;

	public static long getTimePaused() {
		timePaused = timePaused + Math.abs(resume - pause);
		return timePaused;
	}
}
