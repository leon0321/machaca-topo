package com.machacatopo.logica;

import java.util.ArrayList;
/**
 * 
 * @author Leonardo Ortega Hernandez - leon9894@gmail.com
 *
 */
public class Tablero {

	private ArrayList<Herramienta> herramientas = new ArrayList<Herramienta>();
	private int puntajeObtenido = 0;;
	private int intentosRestantes = 10;
	private int tiempo;
	private Herramienta herramientaActiva;
	private static Tablero tablero;

	Tablero() {
		herramientaActiva = new Herramienta(1, Herramienta.DEDO, 1, 1, 1);
		this.herramientas.add(herramientaActiva);
		this.herramientas.add(new Herramienta(1, Herramienta.MARTILLO, 1, 1, 1));
		this.herramientas.add(new Herramienta(2, Herramienta.BOMBA, 1, 1, 3));
		this.herramientas.add(new Herramienta(1, Herramienta.MAZO, 1, 1, 2));
	}

	public ArrayList<Herramienta> getHerramientas() {
		return herramientas;
	}
	
	public Herramienta getHerramienta(int tipo){
		for (int i = 0; i < herramientas.size(); i++) {
			Herramienta h = herramientas.get(i);
			if(h.getTipo() == tipo){
				return h;
			}
		}
		return null;
	}

	public void setHerramientas(ArrayList<Herramienta> herramientas) {
		this.herramientas = herramientas;
	}

	public int getPuntajeObtenido() {
		return puntajeObtenido;
	}

	public void setPuntajeObtenido(int puntajeObtenido) {
		this.puntajeObtenido = puntajeObtenido;
	}

	public int getIntentosRestantes() {
		return intentosRestantes;
	}

	public void setIntentosRestantes(int intentosRestantes) {
		this.intentosRestantes = intentosRestantes;
	}

	public int getTiempo() {
		return tiempo;
	}

	public void setTiempo(int tiempo) {
		this.tiempo = tiempo;
	}

	public Herramienta getHerramientaActiva() {
		return herramientaActiva;
	}

	public void setHerramientaActiva(Herramienta herramientaActiva) {
		this.herramientaActiva = herramientaActiva;
	}

	public void restarIntento() {
		intentosRestantes--;
	}

	public void sumarPuntos(int puntos) {
		puntajeObtenido += puntos;
	}

	public static Tablero get() {
		if (tablero == null) {
			tablero = new Tablero();
		}
		return tablero;
	}

}
