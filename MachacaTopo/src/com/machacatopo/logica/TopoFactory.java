package com.machacatopo.logica;

import java.util.Random;

import android.util.Log;

import com.machacatopo.vista.escena.GameActivity;

/**
 * 
 * @author Leonardo Ortega Hernandez - leon9894@gmail.com
 * 
 */
public class TopoFactory {
	private static TopoFactory instance;
	private Random r;

	private TopoFactory() {
		r = new Random();
	}

	//
	public static TopoFactory getInstance() {
		if (instance == null) {
			Probabilidad.getVariableAleatorea(Probabilidad.tipoTopo);
			instance = new TopoFactory();
		}

		return instance;
	}

	// devuelve el topo que se creo.
	public Topo crearTopo() {
		return createTopo(getTipoTopo());
	}

	public Topo createTopo(int t) {
		if (t == Topo.AGIL) {
			return createTopoAgil();
		}
		if (t == Topo.BASICO) {
			return createTopoBasico();
		}
		if (t == Topo.BONUS) {
			return createTopoBunus();
		}
		if (t == Topo.NINJA) {
			return createTopoNinja();
		}
		if (t == Topo.RESISTENTE) {
			return createTopoResistente();
		}
		return null;
	}

	private Topo createTopoAgil() {
		Topo topo = new TopoAgil();
		topo.setDuracion(3000);
		topo.setCoordenada(Mapa.getInstance().generarPosicion());
		return topo;
	}

	private Topo createTopoNinja() {
		Topo topo = new Topo();
		topo.setTipo(Topo.NINJA);
		topo.setPuntaje(-1);
		topo.setCoordenada(Mapa.getInstance().generarPosicion());
		return topo;
	}

	private Topo createTopoBunus() {
		Topo topo = new Topo();
		topo.setTipo(Topo.BONUS);
		topo.setPuntaje(0);
		topo.setDuracion(1000);
		topo.setCoordenada(Mapa.getInstance().generarPosicion());
		return topo;
	}

	private Topo createTopoBasico() {
		Topo topo = new Topo();
		topo.setCoordenada(Mapa.getInstance().generarPosicion());
		return topo;
	}

	private Topo createTopoResistente() {
		Topo topo = new Topo();
		topo.setTipo(Topo.RESISTENTE);
		topo.setPuntaje(5);
		topo.setDuracion(5000);
		topo.setResistencia(2);
		topo.setCoordenada(Mapa.getInstance().generarPosicion());
		return topo;
	}

	int getTipoTopo() {
		int r1 = r.nextInt(10001);
		float valor = ((float) r1 / 10000f);
		return Probabilidad.simular(Probabilidad.tipoTopo, valor);
	}
}
