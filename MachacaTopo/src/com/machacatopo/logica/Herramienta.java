package com.machacatopo.logica;

import com.machacatopo.logica.tools.C;

/**
 * 
 * @author Leonardo Ortega Hernandez - leon9894@gmail.com
 *
 */
public class Herramienta {

	public static final int BOMBA = C.value();
	public static final int MARTILLO = C.value();
	public static final int MAZO = C.value();
	public static final int DEDO = C.value();
	private int puntosNegativos;
	private int tipo;
	private int cantidadTopoAfecta;
	private int cantidad;
	private int factorIncremento = 1;


	public Herramienta(int puntosNegativos, int tipo, int cantidadTopoAfecta, int cantidad, int factorIncremento) {
		this.puntosNegativos = puntosNegativos;
		this.tipo = tipo;
		this.cantidadTopoAfecta = cantidadTopoAfecta;
		this.cantidad = cantidad;
		this.factorIncremento = factorIncremento;
	}

	public int getPuntosNegativos() {
		return puntosNegativos;
	}

	public void setPuntosNegativos(int puntosNegativos) {
		this.puntosNegativos = puntosNegativos;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public int getCantidadTopoAfecta() {
		return cantidadTopoAfecta;
	}

	public void setCantidadTopoAfecta(int cantidadTopoAfecta) {
		this.cantidadTopoAfecta = cantidadTopoAfecta;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public int getFactorIncremento() {
		return factorIncremento;
	}

	public void setFactorIncremento(int factorIncremento) {
		this.factorIncremento = factorIncremento;
	}

}
