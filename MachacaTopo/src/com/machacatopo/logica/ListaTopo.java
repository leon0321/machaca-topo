package com.machacatopo.logica;

import java.util.ArrayList;
/**
 * 
 * @author Leonardo Ortega Hernandez - leon9894@gmail.com
 *
 */
public class ListaTopo extends ArrayList<Topo> {

	private static ListaTopo instance;

	private ListaTopo() {

	}

	public static ListaTopo getInstance() {

		if (instance == null) {

			instance = new ListaTopo();
		}
		return instance;
	}

	public Topo get(int[] coordenada) {
		for (int i = 0; i < this.size(); i++)
			if (this.get(i).coordenada.equals(coordenada))
				return this.get(i);
		return null;
	}

	public void remove(Topo topo) {
		for (int i = 0; i < this.size(); i++)
			if (this.get(i).equals(topo))
				this.remove(i);
	}
}
