package com.machacatopo.logica;

import com.machacatopo.logica.tools.C;
/**
 * 
 * @author Leonardo Ortega Hernandez - leon9894@gmail.com
 *
 */
public class Topo {
	protected long duracion;
	protected float velocidad;
	protected int puntaje;
	protected boolean visible;
	protected int resistencia;
	protected boolean necesarioDerribar;
	protected int coordenada[];
	protected int habilidad;
	protected int tipo;
	protected boolean vivo = true;
	public final static int BASICO = C.value();
	public final static int AGIL = C.value();
	public final static int RESISTENTE = C.value();
	public final static int NINJA = C.value();
	public final static int BONUS = C.value();

	//
	public Topo(int tipoTopo, int duracion, float velocidad, int puntaje, boolean visible, int resistencia, boolean necesarioDerribar, int[] coordenada, int habilidad) {
		this.duracion = 2000;
		this.velocidad = velocidad;
		this.puntaje = puntaje;
		this.visible = visible;
		this.resistencia = resistencia;
		this.necesarioDerribar = necesarioDerribar;
		this.coordenada = coordenada;
		this.habilidad = habilidad;
		this.tipo = tipoTopo;
	}
	
	public Topo() {
		this.duracion = 2000;
		this.velocidad = 1;
		this.puntaje = 2;
		this.resistencia = 1;
		this.visible = true;
		this.vivo = true;
		this.necesarioDerribar = true;
		this.tipo = BASICO;
	}

	public void restarResisencia(int dano) {

	}

	public long getDuracion() {
		return duracion;
	}

	public void setDuracion(long duracion) {
		this.duracion = duracion;
	}

	public float getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(float velocidad) {
		this.velocidad = velocidad;
	}

	public int getPuntaje() {
		return puntaje;
	}

	public void setPuntaje(int puntaje) {
		this.puntaje = puntaje;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public int getResistencia() {
		return resistencia;
	}

	public void setResistencia(int resistencia) {
		this.resistencia = resistencia;
	}

	public boolean isNecesarioDerribar() {
		return necesarioDerribar;
	}

	public void setNecesarioDerribar(boolean necesarioDerribar) {
		this.necesarioDerribar = necesarioDerribar;
	}

	public int[] getCoordenada() {
		return coordenada;
	}

	public void setCoordenada(int[] coordenada) {
		this.coordenada = coordenada;
	}

	public int getHabilidad() {
		return habilidad;
	}

	public void setHabilidad(int habilidad) {
		this.habilidad = habilidad;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipoTopo) {
		this.tipo = tipoTopo;
	}

	public void golpear(int puntosNegativos) {
		resistencia -= puntosNegativos;
	}
	
	public boolean isVivo(){
		return (resistencia <= 0)?false:true;
	}

}
