package com.machacatopo.logica;
import java.util.Date;
import java.util.Calendar;

import com.machacatopo.logica.tools.C;

/**
 * 
 * @author Leonardo Ortega Hernandez - leon9894@gmail.com
 *
 */
public class Hecho {
	public static final int GOLPE_TOPO = C.value();
	public static final int ESCAPO_TOPO = C.value();
	public static final int GOLPE_TOPO_NINJA = C.value();
	private Topo topo;
	private Date tiempo;
	private Herramienta herramienta;
	private int tipo;
	
	public Hecho(Topo topo,Herramienta herramienta,int tipo){
		//tiempo  = Calendar.getInstance().getTime();
		this.topo = topo;
		this.herramienta = herramienta;
		this.tipo = tipo;
	}

	public Topo getTopo() {
		return topo;
	}

	public void setTopo(Topo topo) {
		this.topo = topo;
	}

	public Date getTiempo() {
		return tiempo;
	}

	public void setTiempo(Date tiempo) {
		this.tiempo = tiempo;
	}

	public Herramienta getHerramienta() {
		return herramienta;
	}

	public void setHerramienta(Herramienta herramienta) {
		this.herramienta = herramienta;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

}
