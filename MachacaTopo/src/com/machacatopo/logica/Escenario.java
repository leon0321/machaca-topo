package com.machacatopo.logica;

/**
 * Clase que se encarga de la gestion de los elementos en el esenario de juego logico
 * @author Leonardo Ortega Hernandez - leon9894@gmail.com
 *
 */

public class Escenario {

	private static Escenario instance;

	private Escenario() {

	}

	public static Escenario getInstance() {
		if (instance == null) {
			instance = new Escenario();
		}
		return instance;
	}

	public Hecho identificarToque(int px, int py) {
		return null;
	}

	public void removerTopo(Topo topo) {
		ListaTopo.getInstance().remove(topo);
		Mapa.getInstance().setElemento(topo.coordenada, Mapa.VACIO);
	}

	public boolean ubicarTopo(Topo topo) {
			Mapa.getInstance().setElemento(topo.coordenada, topo.getTipo());
		return true;
	}
}
