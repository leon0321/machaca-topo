package com.machacatopo.logica;

import java.lang.reflect.Array;
/**
 * 
 * @author Leonardo Ortega Hernandez - leon9894@gmail.com
 *
 */
public class Estadistica {

	private Array ranking;
	private static Estadistica estadistica;
	

	private Estadistica() {

	}

	public static Estadistica get() {
		if (estadistica == null) {
			estadistica = new Estadistica();
		}
		return estadistica;
	}

	public Array getRanking() {
		return ranking;
	}

	public void setRanking(Array ranking) {
		this.ranking = ranking;
	}

	public void actualizarEstadistica() {

	}

}
