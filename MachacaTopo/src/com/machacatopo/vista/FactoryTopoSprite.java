package com.machacatopo.vista;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import com.machacatopo.logica.Escenario;
import com.machacatopo.logica.Mapa;
import com.machacatopo.logica.Topo;
import com.machacatopo.logica.TopoFactory;
import com.machacatopo.vista.escena.GameActivity;

public class FactoryTopoSprite {

	// ===========================================================
	// Constants
	// ===========================================================
	private static FactoryTopoSprite FACTORY_TOPOSPRITE;
	// ===========================================================
	// Fields
	// ===========================================================
	private Timer tiempoProduccion;
	private TimerTask tarea;
	private final long DELAY = 1000;
	private final long PERIOD = 500;
	private boolean pause = false;
	private ArrayList<TopoSprite> topos = new ArrayList<TopoSprite>();

	private FactoryTopoSprite() {
		tarea = new TimerTask() {
			public void run() {
				if (!pause) {
				if (!Mapa.getInstance().isFull()) {
					TopoSprite sprite = crearTopoSprite();
					topos.add(sprite);
					Escenario.getInstance().ubicarTopo(sprite.getTopo());
					GameActivity.get().addSpriteAndTouchArea(sprite);
				}
				}
			}
		};

		tiempoProduccion = new Timer();
	}

	public static FactoryTopoSprite getInstance() {
		if (FACTORY_TOPOSPRITE == null)
			FACTORY_TOPOSPRITE = new FactoryTopoSprite();
		return FACTORY_TOPOSPRITE;
	}

	public TopoSprite crearTopoSprite() {
		Topo topo = TopoFactory.getInstance().crearTopo();
		TopoSprite sprite = new TopoSprite(topo, Mapa.convertirCoorToPx(topo.getCoordenada()), GameActivity.get().getVertexBufferObjectManager());
		return sprite;
	}

	public void start() {
		tiempoProduccion.schedule(tarea, DELAY, PERIOD);
	}

	public void resume() {
		pause = false;
	}

	public void pause() {
		pause = true;
	}
	public void stop(){
		tiempoProduccion.cancel();
	}
	public ArrayList<TopoSprite> getTopos() {
		return topos;
	}
	public void actualizarDuracion(long timePaused) {
		for (int i = 0; i < topos.size(); i++) {
			Topo topo =  topos.get(i).getTopo();
			topo.setDuracion(topo.getDuracion()  + timePaused);
		}
	}
}
