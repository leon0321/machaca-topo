package com.machacatopo.vista.manager;

import org.andengine.entity.text.Text;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.ui.activity.SimpleBaseGameActivity;
import org.andengine.util.color.Color;

import android.graphics.Typeface;

import com.machacatopo.vista.escena.GameActivity;

public class TextManager {
	public static SimpleBaseGameActivity activity;
	private static BitmapTextureAtlas mFontTexture;
	private static Font font;
	public static Text puntaje;
	public static Text intentos;
	public static Text time;

	public static void load(SimpleBaseGameActivity activity) {
		TextManager.activity = activity;
		if (TextManager.activity.getClass() == GameActivity.class) {
			loadTextEcenario();
		}
	}


	private static void loadTextEcenario() {
		mFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.DEFAULT);
		font = new Font(activity.getFontManager(), mFontTexture, Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 25, true, Color.BLACK);
        activity.getTextureManager().loadTexture(mFontTexture);
        activity.getFontManager().loadFont(font);
		//mFontTexture.load();
		puntaje = createText(700, 6, font,"00000000000000000000000000000");
		intentos = createText(700, 30, font, "00000000000000000000000000000");
		time = createText(700, 64, font, "00000000000000000000000000000");
		
	}

	private static Text createText(int x, int y, Font font, String string) {
		return new Text(x, y, font, string, activity.getVertexBufferObjectManager());
	}
}
