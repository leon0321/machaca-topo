package com.machacatopo.vista.manager;

import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.ui.activity.SimpleBaseGameActivity;

import com.machacatopo.logica.Herramienta;
import com.machacatopo.logica.Topo;

public class TexturaManager {

	public static ITiledTextureRegion TEXTURE_TOPOBASICO;
	public static ITiledTextureRegion TEXTURE_TOPOAGIL;
	public static ITiledTextureRegion TEXTURE_TOPORESISTENTE;
	public static ITiledTextureRegion TEXTURE_TOPOBONUS;
	public static ITiledTextureRegion TEXTURE_TOPONINJA;

	public static ITiledTextureRegion TEXTURE_HERRAMEIENTA_BOMBA;

	public static TextureRegion TEXTURE_BACKGROUND_GAME;
	public static TextureRegion TEXTURE_BACKGROUND_MAINMENU;
	public static TextureRegion TEXTURE_BACKGROUND_SPLASH;
	public static TextureRegion TEXTURE_BACKGROUND_SETTINGS;


	public static SimpleBaseGameActivity activity;
	public static TextureRegion TEXTURE_TOOL_BAR;
	public static ITiledTextureRegion TEXTURE_HERRAMEIENTA_MAZO;
	public static TextureRegion TEXTURE_TOOL_LIVE;
	public static TextureRegion TEXTURE_TOOL_SCORE;
   //textures buttons mainmenu
	public static ITiledTextureRegion TEXTURE_BUTTON_PLAY;
	public static ITiledTextureRegion TEXTURE_BUTTON_EXIT;
	public static ITiledTextureRegion TEXTURE_BUTTON_CONFIG;
	public static ITiledTextureRegion TEXTURE_BUTTON_RANKING;
	//textures buttons settings
	public static ITiledTextureRegion TEXTURE_BUTTON_SOUND;

	
	
	

	public static void load(SimpleBaseGameActivity activity) {
		TexturaManager.activity = activity;
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		loadTextureViewGame();
		loadTextureMainMenu();
		loadTextureSplash();
		loadTextureSettings();
	}

	private static void loadTextureViewGame() {
		// Textura de los topos
		TEXTURE_TOPOBASICO = loadTextureAnimated("topo/basico.png", 232, 232, 4, 4);
		TEXTURE_TOPOAGIL = loadTextureAnimated("topo/agil.png", 232, 232, 4, 4);
		TEXTURE_TOPORESISTENTE = loadTextureAnimated("topo/resistente.png", 232, 232, 4, 4);
		TEXTURE_TOPOBONUS = loadTextureAnimated("topo/bonus.png", 232, 232, 4, 4);
		TEXTURE_TOPONINJA = loadTextureAnimated("topo/ninja.png", 232, 232, 4, 4);
		// Textura de las herramientas
		TEXTURE_HERRAMEIENTA_BOMBA = loadTextureAnimated("tool/bomba.png", 240, 60, 1, 4);
		TEXTURE_HERRAMEIENTA_MAZO = loadTextureAnimated("tool/maza.png", 240, 60, 1, 4);
		//Textura de barra de herrameintas
		TEXTURE_TOOL_BAR = loadTexture("background/barraherramientas.png", 74, 220);
		TEXTURE_TOOL_LIVE = loadTexture("background/live.png", 22, 18);
		TEXTURE_TOOL_SCORE = loadTexture("background/tablero.png", 215, 66);
		// Textura del fondo
		TEXTURE_BACKGROUND_GAME = loadTexture("background/fondo.png", 800, 458);
	}

	private static void loadTextureMainMenu() {
		TEXTURE_BACKGROUND_MAINMENU = loadTexture("background/background_mainmenu.png", 1080, 648);
		// Textura de botones
		TEXTURE_BUTTON_PLAY = loadTextureAnimated("button/boton_play.png", 344,117, 1, 1);
		TEXTURE_BUTTON_CONFIG = loadTextureAnimated("button/boton_config.png", 344,117, 1, 1);
		TEXTURE_BUTTON_RANKING = loadTextureAnimated("button/boton_ranking.png", 344,117, 1, 1);
		TEXTURE_BUTTON_EXIT = loadTextureAnimated("button/boton_exit.png", 58,59, 1, 1);

	}
	
	private static void loadTextureSplash(){
		TEXTURE_BACKGROUND_SPLASH = loadTexture("background/splash.png", 800, 480);
	}
	
	private static void loadTextureSettings(){
		//background
		TEXTURE_BACKGROUND_SETTINGS = loadTexture("background/settings.png", 800, 480);
		//buttons
		TEXTURE_BUTTON_SOUND = loadTextureAnimated("button/button_sound.png", 268,112, 1, 2);
	}

	private static ITiledTextureRegion loadTextureAnimated(String url, int w, int h, int row, int column) {
		BitmapTextureAtlas atlas = createAtlas(w, h);
		ITiledTextureRegion region = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(atlas, activity, url, 0, 0, column, row);
		atlas.load();
		return region;
	}

	private static TextureRegion loadTexture(String url, int w, int h) {
		BitmapTextureAtlas atlas = createAtlas(w, h);
		TextureRegion region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(atlas, activity, url, 0, 0);
		atlas.load();
		return region;
	}

	private static BitmapTextureAtlas createAtlas(int w, int h) {
		return new BitmapTextureAtlas(activity.getTextureManager(), cuadrado(w), cuadrado(h), TextureOptions.DEFAULT);
	}

	private static int cuadrado(int numero) {
		double exp = Math.log(numero) / Math.log(2);
		int expInt = (int) exp;
		double dif = exp - expInt;
		if (dif == 0) {
			return numero;
		}
		numero = (int) Math.pow(2, expInt + 1);
		return numero;
	}

	private static void loadTextureRanking() {

	}

	public static ITiledTextureRegion getTextura(final int tipo) {

		if (Topo.BASICO == tipo) {
			return TEXTURE_TOPOBASICO;
		} else if (Topo.AGIL == tipo) {
			return TEXTURE_TOPOAGIL;
		} else if (Topo.NINJA == tipo) {
			return TEXTURE_TOPONINJA;
		} else if (Topo.BONUS == tipo) {
			return TEXTURE_TOPOBONUS;
		} else if (Topo.RESISTENTE == tipo) {
			return TEXTURE_TOPORESISTENTE;
		} else if (Herramienta.BOMBA == tipo) {
			return TEXTURE_HERRAMEIENTA_BOMBA;
		} else if (Herramienta.MAZO == tipo) {
			return TEXTURE_HERRAMEIENTA_MAZO;
		}
		return null;
	}
}
