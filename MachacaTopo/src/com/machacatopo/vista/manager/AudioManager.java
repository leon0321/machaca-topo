package com.machacatopo.vista.manager;

import java.io.IOException;

import org.andengine.audio.music.Music;
import org.andengine.audio.music.MusicFactory;
import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.ui.activity.SimpleBaseGameActivity;
import org.andengine.util.debug.Debug;

import com.machacatopo.vista.escena.GameActivity;

public class AudioManager {
	public static Sound GOLPE;
	public static Music TAMBORES;
	public static SimpleBaseGameActivity activity;

	public static void load(SimpleBaseGameActivity activity) {
		AudioManager.activity = activity;
		SoundFactory.setAssetBasePath("mfx/");
		MusicFactory.setAssetBasePath("mfx/");
		if (AudioManager.activity.getClass() == GameActivity.class) {
			loadAudioEcenario();
		} 
	}

	private static void loadAudioRanking() {
		// TODO Auto-generated method stub

	}

	private static void loadAudioMenu() {
		// TODO Auto-generated method stub
	}

	private static void loadAudioEcenario() {
		GOLPE = loadSound("efectts/whirrp.mp3");
		TAMBORES = loadMusic("music/sonido_rapido.mp3");
	}

	private static Music loadMusic(String url) {
		try {
			Music music = MusicFactory.createMusicFromAsset(activity.getMusicManager(), activity, url);
			music.setLooping(true);
			music.setVolume(1f);
			return music;
		} catch (final IOException e) {
			Debug.e(e);
			return null;
		}

	}

	private static Sound loadSound(String url) {
		try {
			return SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, url);
		} catch (final IOException e) {
			Debug.e(e);
		}
		return null;
	}
}
