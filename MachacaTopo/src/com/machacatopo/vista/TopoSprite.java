package com.machacatopo.vista;

import java.util.Date;

import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.machacatopo.logica.Escenario;
import com.machacatopo.logica.Hecho;
import com.machacatopo.logica.LogicaJuego;
import com.machacatopo.logica.Tablero;
import com.machacatopo.logica.Topo;
import com.machacatopo.vista.escena.GameActivity;
import com.machacatopo.vista.manager.AudioManager;
import com.machacatopo.vista.manager.TextManager;
import com.machacatopo.vista.manager.TexturaManager;

/**
 * 
 * @author Leonardo Ortega Hernandez - leon9894@gmail.com
 * 
 *
 */
public class TopoSprite extends AnimatedSprite {
	private Topo topo = null;
	private boolean down = false;
	private long timei = 0;

	public TopoSprite(Topo topo, int[] coord, VertexBufferObjectManager vertexBufferObjectManager) {
		super(coord[0], coord[1], TexturaManager.getTextura(topo.getTipo()), vertexBufferObjectManager);
		this.topo = topo;
		long aniamtion[] = { 200, 200, 200, 200 };
		this.animate(aniamtion, 1, 4, true);
		timei = new Date().getTime();
		//Log.i("creado: ", "" + (timei.getTime()));


	}

	/**
	 * Este metodo dectecta los eventos de touch sobre el sprite
	 * 
	 * @author leon
	 * @param pSceneTouchEvent
	 * @param pTouchAreaLocalX
	 * @param pTouchAreaLocalY
	 * @return boolean
	 */
	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {

		if (TouchEvent.ACTION_DOWN == pSceneTouchEvent.getAction()) {
			down = true;
			AudioManager.GOLPE.play();
			this.setScale(2f);
			LogicaJuego.get().aplicarRegla(new Hecho(topo, Tablero.get().getHerramientaActiva(), Hecho.GOLPE_TOPO));
			if (LogicaJuego.FIN_PARTIDA) {
				FactoryTopoSprite.getInstance().pause();
			}
		} else if (down) {
			this.setScale(1f);
			TextManager.intentos.setText("" + Tablero.get().getIntentosRestantes());
			TextManager.puntaje.setText("" + Tablero.get().getPuntajeObtenido());
			TextManager.intentos.setX(800f - TextManager.intentos.getWidth());
			TextManager.puntaje.setX(800f - TextManager.puntaje.getWidth());

			if (!topo.isVivo()) {
				remover();
			}
		}
		return true;
	}

	public Topo getTopo() {
		return topo;
	}
public long getTimei() {
	return timei;
}
	public void remover() {
		GameActivity.get().removeSpriteAndTouchArea(this);
		Escenario.getInstance().removerTopo(topo);
	}

	public void setTopo(Topo topo) {
		this.topo = topo;
	}

}
