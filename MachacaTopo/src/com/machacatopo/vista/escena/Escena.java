package com.machacatopo.vista.escena;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.controller.MultiTouch;
import org.andengine.ui.activity.SimpleBaseGameActivity;

import android.widget.Toast;

import com.machacatopo.vista.manager.AudioManager;
import com.machacatopo.vista.manager.TexturaManager;


public class Escena extends SimpleBaseGameActivity {

	protected Scene scene;
	public static int WIDTH = 800;
	public static int HEIGHT = 480;
	private Camera camera;

	@Override
	public EngineOptions onCreateEngineOptions() {
		// Definimos nuestra camara
		this.camera = new Camera(0, 0, WIDTH, HEIGHT);
		// Ahora declaramos las opciones del motor
		EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new FillResolutionPolicy(), camera);
		// EngineOptions(es full screen?, Cual es la orientacion de la
		// pantalla?, Como actuaremos ante distintas resoluciones?, y la camara)
		// impedimos que la pantalla se apague por inactividad
		engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON);
		// habilita multiTuoch
		//Habilitamos los sonidos y la musica
		engineOptions.getAudioOptions().setNeedsSound(true);
		engineOptions.getAudioOptions().setNeedsMusic(true);
		// Return the engineOptions object, passing it to the engine
		return engineOptions;
	}

	@Override
	protected void onCreateResources() {
		TexturaManager.load(this);
		AudioManager.load(this);
	}

	@Override
	protected Scene onCreateScene() {
		this.scene = new Scene();
		return this.scene;
	}



	public void addSpriteAndTouchArea(Sprite sprite) {
		scene.attachChild(sprite);
		scene.registerTouchArea(sprite);
	}
	
	public void removeSpriteAndTouchArea(Sprite sprite) {
		if (scene.detachChild(sprite)) {
			scene.getTouchAreas().remove(sprite);
		}
	}
	
	protected void enableMultiTouch(EngineOptions engineOptions) {
		engineOptions.getTouchOptions().setNeedsMultiTouch(true);
		if (MultiTouch.isSupported(this)) {
			if (MultiTouch.isSupportedDistinct(this)) {
				Toast.makeText(this, "MultiTouch detected --> Both controls will work properly!", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(this, "MultiTouch detected, but your device has problems distinguishing between fingers.\n\nControls are placed at different vertical locations.", Toast.LENGTH_LONG).show();
			}
		} else {
			Toast.makeText(this, "Sorry your device does NOT support MultiTouch!\n\n(Falling back to SingleTouch.)\n\nControls are placed at different vertical locations.", Toast.LENGTH_LONG).show();
		}
	}
}
