package com.machacatopo.vista.escena;

import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.SpriteBackground;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.ui.activity.SimpleBaseGameActivity;

import com.machacatopo.logica.LogicaJuego;
import com.machacatopo.vista.manager.AudioManager;
import com.machacatopo.vista.manager.TexturaManager;

public class MainMenu extends Scene {
	private static MainMenu scene;
	AnimatedSprite buttonPlay;
	AnimatedSprite buttonExit;
	AnimatedSprite buttonRaking;
	AnimatedSprite buttonConfig;
	SpriteBackground spriteBackground;

	public MainMenu() {
		scene = this;
		SimpleBaseGameActivity activity = GameActivity.get();
		spriteBackground = new SpriteBackground(new Sprite(0, 0, TexturaManager.TEXTURE_BACKGROUND_MAINMENU, activity.getVertexBufferObjectManager()));
		spriteBackground.getSprite().setWidth(Escena.WIDTH);
		spriteBackground.getSprite().setHeight(Escena.HEIGHT);
		createButtons();
		// Registro en la escena los sprite
		this.setBackground(spriteBackground);
		this.attachChild(buttonPlay);
		this.attachChild(buttonExit);
		this.attachChild(buttonConfig);
		this.attachChild(buttonRaking);
		this.registerTouchArea(buttonExit);
		this.registerTouchArea(buttonPlay);
		this.registerTouchArea(buttonConfig);
		this.registerTouchArea(buttonRaking);
		// musica
		AudioManager.TAMBORES.play();
	}

	public void createButtons() {
		buttonPlay = new AnimatedSprite(0, 120, TexturaManager.TEXTURE_BUTTON_PLAY, GameActivity.get().getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent touchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (TouchEvent.ACTION_UP == touchEvent.getAction()) {
					if (LogicaJuego.FIN_PARTIDA) {
						LogicaJuego.get().newGame();
					}
					ViewGame.getScene();
					GameActivity.get().setCurrentScene(ViewGame.getScene());
					ViewGame.getScene().onResumeGame();
				}
				return true;
			}
		};
		buttonPlay.setX(Escena.WIDTH / 2 - buttonPlay.getWidth() / 2);

		buttonRaking = new AnimatedSprite(0, 240, TexturaManager.TEXTURE_BUTTON_RANKING, GameActivity.get().getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent touchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (TouchEvent.ACTION_UP == touchEvent.getAction()) {
					System.exit(0);
				}
				return true;
			}
		};
		buttonRaking.setX(Escena.WIDTH / 2 - buttonRaking.getWidth() / 2);

		buttonConfig = new AnimatedSprite(0, 360, TexturaManager.TEXTURE_BUTTON_CONFIG, GameActivity.get().getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent touchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (TouchEvent.ACTION_UP == touchEvent.getAction()) {
					GameActivity.get().setCurrentScene(ViewSettings.get());
				}
				return true;
			}
		};
		buttonConfig.setX(Escena.WIDTH / 2 - buttonConfig.getWidth() / 2);

		buttonExit = new AnimatedSprite(0, 410, TexturaManager.TEXTURE_BUTTON_EXIT, GameActivity.get().getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent touchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (TouchEvent.ACTION_UP == touchEvent.getAction()) {
					System.exit(0);
				}
				return true;
			}
		};

	}

	public static MainMenu getScene() {
		if (scene == null) {
			scene = new MainMenu();
		}
		return scene;
	}

}
