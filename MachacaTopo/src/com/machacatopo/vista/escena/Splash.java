package com.machacatopo.vista.escena;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.SpriteBackground;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.util.modifier.IModifier;

import com.machacatopo.vista.manager.TexturaManager;

public class Splash extends Scene{
	
	public Splash(){
		SpriteBackground spriteBackground = new SpriteBackground(new Sprite(0, 0, TexturaManager.TEXTURE_BACKGROUND_SPLASH, GameActivity.get().getVertexBufferObjectManager()));
		setBackground(spriteBackground);

		DelayModifier dMod = new DelayModifier(5,
				new IEntityModifierListener() {

			

					@Override
					public void onModifierFinished(IModifier<IEntity> arg0, IEntity arg1) {
						GameActivity.get().setCurrentScene(MainMenu.getScene());						
					}

					@Override
					public void onModifierStarted(IModifier<IEntity> arg0, IEntity arg1) {
						// TODO Auto-generated method stub
						
					}
				});
		registerEntityModifier(dMod);

	}
}
