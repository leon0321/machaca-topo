package com.machacatopo.vista.escena;

import java.util.ArrayList;
import java.util.Date;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.SpriteBackground;
import org.andengine.entity.sprite.Sprite;
import org.andengine.ui.activity.SimpleBaseGameActivity;

import android.util.Log;

import com.machacatopo.logica.Escenario;
import com.machacatopo.logica.Hecho;
import com.machacatopo.logica.Herramienta;
import com.machacatopo.logica.LogicaJuego;
import com.machacatopo.logica.Tablero;
import com.machacatopo.logica.tools.Time;
import com.machacatopo.vista.FactoryTopoSprite;
import com.machacatopo.vista.SpriteHerramienta;
import com.machacatopo.vista.TopoSprite;
import com.machacatopo.vista.manager.AudioManager;
import com.machacatopo.vista.manager.TextManager;
import com.machacatopo.vista.manager.TexturaManager;

public class ViewGame extends Scene {
	private Sprite toolBar;
	private Sprite score;
	private static ViewGame scene;

	public ViewGame() {
		Time.timeGame = new Date().getTime();
		SimpleBaseGameActivity activity = GameActivity.get();
		// Sprite
		SpriteBackground spriteBackground = new SpriteBackground(new Sprite(0, 0, TexturaManager.TEXTURE_BACKGROUND_GAME, activity.getVertexBufferObjectManager()));
		SpriteHerramienta bomba = new SpriteHerramienta(Tablero.get().getHerramienta(Herramienta.BOMBA), new int[] {
				0, 0 }, activity.getVertexBufferObjectManager());
		SpriteHerramienta mazo = new SpriteHerramienta(Tablero.get().getHerramienta(Herramienta.MAZO), new int[] {
				0, 0 }, activity.getVertexBufferObjectManager());
		toolBar = new Sprite(0, 0, TexturaManager.TEXTURE_TOOL_BAR, activity.getVertexBufferObjectManager());
		score = new Sprite(700, 0, TexturaManager.TEXTURE_TOOL_SCORE, activity.getVertexBufferObjectManager());
		score.setScaleY(1.2f);
		Sprite live = new Sprite(40, 35, TexturaManager.TEXTURE_TOOL_LIVE, activity.getVertexBufferObjectManager());
		score.attachChild(live);

		toolBar.attachChild(bomba);
		toolBar.attachChild(mazo);
		bomba.setPosition(5, 70);
		mazo.setPosition(5, 10);

		// Registro en la escena de lo sprite
		this.setBackground(spriteBackground);
		this.attachChild(toolBar);
		this.attachChild(score);

		this.registerTouchArea(bomba);
		this.registerTouchArea(mazo);
		// Texto
		this.attachChild(TextManager.intentos);
		this.attachChild(TextManager.time);
		// update scene
		init();
		// musica
	}

	public void init() {
		this.registerUpdateHandler(new IUpdateHandler() {

			@Override
			public void reset() {
			}

			@Override
			public void onUpdate(float arg0) {
				if (LogicaJuego.FIN_PARTIDA) {
					removeAllTopos();
					return;
				}
				long timef = new Date().getTime();

				TextManager.time.setText("" + ((timef - Time.timeGame) / 1000));
				TextManager.time.setPosition(800f - TextManager.time.getWidth(), 64);
				for (int i = 0; i < getChildCount(); i++) {
					if (getChildByIndex(i).getClass() == TopoSprite.class) {
						TopoSprite topo = (TopoSprite) getChildByIndex(i);
						long d = timef - (topo.getTimei());
						if (d >= (topo.getTopo().getDuracion())) {
							LogicaJuego.get().aplicarRegla(new Hecho(topo.getTopo(), null, Hecho.ESCAPO_TOPO));
							removeSpriteAndTouchArea(topo);
							Escenario.getInstance().removerTopo(topo.getTopo());
						}
					}
				}
				TextManager.intentos.setText("" + Tablero.get().getIntentosRestantes());
				TextManager.intentos.setX(800f - TextManager.intentos.getWidth());
			}
		});
	}

	public void removeAllTopos() {
		ArrayList<TopoSprite> topos = FactoryTopoSprite.getInstance().getTopos();
		for (int i = 0; i < topos.size(); i++) {
			removeSpriteAndTouchArea(topos.get(i));
			Escenario.getInstance().removerTopo(topos.get(i).getTopo());
		}
	}

	public void removeSpriteAndTouchArea(Sprite sprite) {
		if (this.detachChild(sprite)) {
			this.getTouchAreas().remove(sprite);
		}
	}

	void onResumeGame() {
		if (Time.pause == 0 || Time.resume != 0) {
			return;
		}
		Date date = new Date();
		Time.resume = date.getTime();
		Time.getTimePaused();
		FactoryTopoSprite.getInstance().actualizarDuracion((Time.resume - Time.pause));
		Time.pause = 0;
		Time.resume = 0;
		if (!LogicaJuego.FIN_PARTIDA) {
			FactoryTopoSprite.getInstance().resume();
		}

	}

	public synchronized void onPauseGame() {
		FactoryTopoSprite.getInstance().pause();
		Date date = new Date();
		Time.pause = date.getTime();
	}

	public static ViewGame getScene() {
		if (scene == null) {
			scene = new ViewGame();
		}
		return scene;
	}

}
