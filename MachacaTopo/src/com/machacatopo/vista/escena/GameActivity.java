package com.machacatopo.vista.escena;

import java.util.Date;

import org.andengine.engine.options.EngineOptions;
import org.andengine.entity.scene.Scene;

import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import com.machacatopo.logica.tools.Time;
import com.machacatopo.vista.FactoryTopoSprite;
import com.machacatopo.vista.manager.TextManager;

//
public class GameActivity extends Escena {

	private static GameActivity INSTANCE;

	public static void setVistaEscenario(GameActivity menuPrincipal) {
		INSTANCE = menuPrincipal;
	}

	public static GameActivity get() {
		return INSTANCE;
	}

	private long timei;

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	@Override
	public EngineOptions onCreateEngineOptions() {
		EngineOptions engineOptions = super.onCreateEngineOptions();
		enableMultiTouch(engineOptions);
		// Return the engineOptions object, passing it to the engine
		return engineOptions;
	}

	@Override
	protected void onCreateResources() {
		super.onCreateResources();
		timei = new Date().getTime();
		TextManager.load(this);
		GameActivity.setVistaEscenario(this);
		FactoryTopoSprite.getInstance().start();
		ViewGame.getScene().onPauseGame();
	}


	@Override
	protected Scene onCreateScene() {
		super.onCreateScene();
		scene = new Splash();
		return scene;
	}

	// ===========================================================
	// Methods
	// ===========================================================

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			ViewGame.getScene().onPauseGame();
			setCurrentScene(MainMenu.getScene());
		} 
		return false;
	}



	public long getTimei() {
		return timei;
	}

	public  void setCurrentScene(Scene scene) {
		this.scene = scene;
		this.getEngine().setScene(scene);		
	}
}