package com.machacatopo.vista.escena;

import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.SpriteBackground;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.input.touch.TouchEvent;

import android.R.bool;

import com.machacatopo.vista.manager.AudioManager;
import com.machacatopo.vista.manager.TexturaManager;

public class ViewSettings extends Scene {
	SpriteBackground spriteBackground;
	private AnimatedSprite buttonExit;
	private TiledSprite buttonSound;
	private static ViewSettings settings;

	public ViewSettings() {
		spriteBackground = new SpriteBackground(new Sprite(0, 0, TexturaManager.TEXTURE_BACKGROUND_SETTINGS, GameActivity.get().getVertexBufferObjectManager()));
		setBackground(spriteBackground);
		spriteBackground.getSprite().setWidth(Escena.WIDTH);
		spriteBackground.getSprite().setHeight(Escena.HEIGHT);
		createButtons();
		// Registro en la escena los sprite
		this.setBackground(spriteBackground);
		this.attachChild(buttonExit);
		this.attachChild(buttonSound);
		this.registerTouchArea(buttonExit);
		this.registerTouchArea(buttonSound);
		// musica
		AudioManager.TAMBORES.play();
	}

	public void createButtons() {
		buttonSound = new TiledSprite(60, 50, TexturaManager.TEXTURE_BUTTON_SOUND, GameActivity.get().getVertexBufferObjectManager()) {
			private boolean p = false;
			@Override
			public boolean onAreaTouched(TouchEvent touchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (TouchEvent.ACTION_UP == touchEvent.getAction()) {
					if(!p){
						AudioManager.TAMBORES.pause();
						this.setCurrentTileIndex(1);
					}else{
						AudioManager.TAMBORES.play();
						this.setCurrentTileIndex(0);
					}
					p = !p;
				}
				return true;
			}
		};

		buttonExit = new AnimatedSprite(700, 30, TexturaManager.TEXTURE_BUTTON_EXIT, GameActivity.get().getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent touchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (TouchEvent.ACTION_UP == touchEvent.getAction()) {
					GameActivity.get().setCurrentScene(MainMenu.getScene());
				}
				return true;
			}
		};

	}
	public static ViewSettings get(){
		if(settings == null){
			settings = new ViewSettings();
		}
		return settings;
	}
}
