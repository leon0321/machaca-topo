package com.machacatopo.vista;

import java.util.Date;

import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.util.Log;

import com.machacatopo.logica.Herramienta;
import com.machacatopo.logica.Tablero;
import com.machacatopo.logica.tools.Time;
import com.machacatopo.vista.escena.GameActivity;
import com.machacatopo.vista.manager.TexturaManager;

public class SpriteHerramienta extends AnimatedSprite {
	private Herramienta herramienta;
	private long[] animationTime = { 200, 200, 200, 200 };

	public SpriteHerramienta(Herramienta herramienta, int[] coord, VertexBufferObjectManager vertexBufferObjectManager) {
		super(0, 0, TexturaManager.getTextura(herramienta.getTipo()), vertexBufferObjectManager);
		this.herramienta = herramienta;

		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean onAreaTouched(TouchEvent touchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
		if (TouchEvent.ACTION_DOWN == touchEvent.getAction()) {
			Log.i("Touch", "HERRAMEINTA : " + Herramienta.BOMBA + "  -  " + herramienta.getTipo());
			
			Tablero.get().setHerramientaActiva(herramienta);
			GameActivity.get().onPauseGame();
			this.animate(200, true);
			
		}

		return false;
	}
}
